import pandas as pd

data = pd.read_csv(r'sample.csv')
sample_df = pd.DataFrame(data, columns=['brand', 'model', 'type', 'year'])

data = pd.read_csv(r'brand.csv')
brand_df = pd.DataFrame(data, columns=['brand', 'id'])
brand_df = brand_df.set_index('id')

model_list = []
for brand in brand_df['brand']:
    df = sample_df[sample_df['brand'] == brand]
    model_list.append(df['model'].value_counts()[:10].index.tolist())
models = []
for model_a in model_list:
    for model in model_a:
        models.append(model)
ids = []
brand_ids = []
for id in range(1, 41):
    ids.append(id)
for i in range(1, 5):
    for j in range(1, 11):
        brand_ids.append(i)

data = {
    'id': ids,
    'brand_id': brand_ids,
    'model': models
}
dataframe = pd.DataFrame(data=data)
dataframe.to_csv('model.csv',columns=['id', 'brand_id', 'model'], index=False)